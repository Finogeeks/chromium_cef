// Copyright (c) 2023 The Chromium Embedded Framework Authors. All rights
// reserved. Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.
//
// ---------------------------------------------------------------------------
//
// This file was generated by the CEF translator tool. If making changes by
// hand only do so within the body of existing method and function
// implementations. See the translator.README.txt file in the tools directory
// for more information.
//
// $hash=7af372362be16cd5150da026dbbf41c85daeba88$
//

#include "libcef_dll/ctocpp/browser_permission_request_delegate_ctocpp.h"
#include "libcef_dll/shutdown_checker.h"

// VIRTUAL METHODS - Body may be edited by hand.

NO_SANITIZE("cfi-icall")
void CefBrowserPermissionRequestDelegateCToCpp::AskGeolocationPermission(
    const CefString& origin,
    cef_permission_callback_t callback) {
  shutdown_checker::AssertNotShutdown();

  cef_browser_permission_request_delegate_t* _struct = GetStruct();
  if (CEF_MEMBER_MISSING(_struct, ask_geolocation_permission))
    return;

  // AUTO-GENERATED CONTENT - DELETE THIS COMMENT BEFORE MODIFYING

  // Verify param: origin; type: string_byref_const
  DCHECK(!origin.empty());
  if (origin.empty())
    return;

  // Execute
  _struct->ask_geolocation_permission(_struct, origin.GetStruct(), callback);
}

NO_SANITIZE("cfi-icall")
void CefBrowserPermissionRequestDelegateCToCpp::AbortAskGeolocationPermission(
    const CefString& origin) {
  shutdown_checker::AssertNotShutdown();

  cef_browser_permission_request_delegate_t* _struct = GetStruct();
  if (CEF_MEMBER_MISSING(_struct, abort_ask_geolocation_permission))
    return;

  // AUTO-GENERATED CONTENT - DELETE THIS COMMENT BEFORE MODIFYING

  // Verify param: origin; type: string_byref_const
  DCHECK(!origin.empty());
  if (origin.empty())
    return;

  // Execute
  _struct->abort_ask_geolocation_permission(_struct, origin.GetStruct());
}

NO_SANITIZE("cfi-icall")
void CefBrowserPermissionRequestDelegateCToCpp::
    AskProtectedMediaIdentifierPermission(const CefString& origin,
                                          cef_permission_callback_t callback) {
  shutdown_checker::AssertNotShutdown();

  cef_browser_permission_request_delegate_t* _struct = GetStruct();
  if (CEF_MEMBER_MISSING(_struct, ask_protected_media_identifier_permission))
    return;

  // AUTO-GENERATED CONTENT - DELETE THIS COMMENT BEFORE MODIFYING

  // Verify param: origin; type: string_byref_const
  DCHECK(!origin.empty());
  if (origin.empty())
    return;

  // Execute
  _struct->ask_protected_media_identifier_permission(
      _struct, origin.GetStruct(), callback);
}

NO_SANITIZE("cfi-icall")
void CefBrowserPermissionRequestDelegateCToCpp::
    AbortAskProtectedMediaIdentifierPermission(const CefString& origin) {
  shutdown_checker::AssertNotShutdown();

  cef_browser_permission_request_delegate_t* _struct = GetStruct();
  if (CEF_MEMBER_MISSING(_struct,
                         abort_ask_protected_media_identifier_permission))
    return;

  // AUTO-GENERATED CONTENT - DELETE THIS COMMENT BEFORE MODIFYING

  // Verify param: origin; type: string_byref_const
  DCHECK(!origin.empty());
  if (origin.empty())
    return;

  // Execute
  _struct->abort_ask_protected_media_identifier_permission(_struct,
                                                           origin.GetStruct());
}

NO_SANITIZE("cfi-icall")
void CefBrowserPermissionRequestDelegateCToCpp::AskMIDISysexPermission(
    const CefString& origin,
    cef_permission_callback_t callback) {
  shutdown_checker::AssertNotShutdown();

  cef_browser_permission_request_delegate_t* _struct = GetStruct();
  if (CEF_MEMBER_MISSING(_struct, ask_midisysex_permission))
    return;

  // AUTO-GENERATED CONTENT - DELETE THIS COMMENT BEFORE MODIFYING

  // Verify param: origin; type: string_byref_const
  DCHECK(!origin.empty());
  if (origin.empty())
    return;

  // Execute
  _struct->ask_midisysex_permission(_struct, origin.GetStruct(), callback);
}

NO_SANITIZE("cfi-icall")
void CefBrowserPermissionRequestDelegateCToCpp::AbortAskMIDISysexPermission(
    const CefString& origin) {
  shutdown_checker::AssertNotShutdown();

  cef_browser_permission_request_delegate_t* _struct = GetStruct();
  if (CEF_MEMBER_MISSING(_struct, abort_ask_midisysex_permission))
    return;

  // AUTO-GENERATED CONTENT - DELETE THIS COMMENT BEFORE MODIFYING

  // Verify param: origin; type: string_byref_const
  DCHECK(!origin.empty());
  if (origin.empty())
    return;

  // Execute
  _struct->abort_ask_midisysex_permission(_struct, origin.GetStruct());
}

NO_SANITIZE("cfi-icall")
void CefBrowserPermissionRequestDelegateCToCpp::NotifyGeolocationPermission(
    bool value,
    const CefString& origin) {
  shutdown_checker::AssertNotShutdown();

  cef_browser_permission_request_delegate_t* _struct = GetStruct();
  if (CEF_MEMBER_MISSING(_struct, notify_geolocation_permission))
    return;

  // AUTO-GENERATED CONTENT - DELETE THIS COMMENT BEFORE MODIFYING

  // Verify param: origin; type: string_byref_const
  DCHECK(!origin.empty());
  if (origin.empty())
    return;

  // Execute
  _struct->notify_geolocation_permission(_struct, value, origin.GetStruct());
}

// CONSTRUCTOR - Do not edit by hand.

CefBrowserPermissionRequestDelegateCToCpp::
    CefBrowserPermissionRequestDelegateCToCpp() {}

// DESTRUCTOR - Do not edit by hand.

CefBrowserPermissionRequestDelegateCToCpp::
    ~CefBrowserPermissionRequestDelegateCToCpp() {
  shutdown_checker::AssertNotShutdown();
}

template <>
cef_browser_permission_request_delegate_t*
CefCToCppRefCounted<CefBrowserPermissionRequestDelegateCToCpp,
                    CefBrowserPermissionRequestDelegate,
                    cef_browser_permission_request_delegate_t>::
    UnwrapDerived(CefWrapperType type, CefBrowserPermissionRequestDelegate* c) {
  NOTREACHED() << "Unexpected class type: " << type;
  return nullptr;
}

template <>
CefWrapperType CefCToCppRefCounted<
    CefBrowserPermissionRequestDelegateCToCpp,
    CefBrowserPermissionRequestDelegate,
    cef_browser_permission_request_delegate_t>::kWrapperType =
    WT_BROWSER_PERMISSION_REQUEST_DELEGATE;
