// Copyright (c) 2023 The Chromium Embedded Framework Authors. All rights
// reserved. Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.
//
// ---------------------------------------------------------------------------
//
// This file was generated by the CEF translator tool. If making changes by
// hand only do so within the body of existing method and function
// implementations. See the translator.README.txt file in the tools directory
// for more information.
//
// $hash=3a61e1e63ad6832db1385c4d5fa24a772054a09a$
//

#include "libcef_dll/ctocpp/date_time_chooser_callback_ctocpp.h"
#include "libcef_dll/shutdown_checker.h"

// VIRTUAL METHODS - Body may be edited by hand.

NO_SANITIZE("cfi-icall")
void CefDateTimeChooserCallbackCToCpp::Continue(bool success,
                                                double dialog_value) {
  shutdown_checker::AssertNotShutdown();

  cef_date_time_chooser_callback_t* _struct = GetStruct();
  if (CEF_MEMBER_MISSING(_struct, cont))
    return;

  // AUTO-GENERATED CONTENT - DELETE THIS COMMENT BEFORE MODIFYING

  // Execute
  _struct->cont(_struct, success, dialog_value);
}

// CONSTRUCTOR - Do not edit by hand.

CefDateTimeChooserCallbackCToCpp::CefDateTimeChooserCallbackCToCpp() {}

// DESTRUCTOR - Do not edit by hand.

CefDateTimeChooserCallbackCToCpp::~CefDateTimeChooserCallbackCToCpp() {
  shutdown_checker::AssertNotShutdown();
}

template <>
cef_date_time_chooser_callback_t* CefCToCppRefCounted<
    CefDateTimeChooserCallbackCToCpp,
    CefDateTimeChooserCallback,
    cef_date_time_chooser_callback_t>::UnwrapDerived(CefWrapperType type,
                                                     CefDateTimeChooserCallback*
                                                         c) {
  NOTREACHED() << "Unexpected class type: " << type;
  return nullptr;
}

template <>
CefWrapperType
    CefCToCppRefCounted<CefDateTimeChooserCallbackCToCpp,
                        CefDateTimeChooserCallback,
                        cef_date_time_chooser_callback_t>::kWrapperType =
        WT_DATE_TIME_CHOOSER_CALLBACK;
