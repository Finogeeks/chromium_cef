// Copyright (c) 2023 The Chromium Embedded Framework Authors. All rights
// reserved. Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.
//
// ---------------------------------------------------------------------------
//
// This file was generated by the CEF translator tool. If making changes by
// hand only do so within the body of existing method and function
// implementations. See the translator.README.txt file in the tools directory
// for more information.
//
// $hash=19ab4c6d715b63d1acb26150828c87d08f2b188e$
//

#ifndef CEF_LIBCEF_DLL_CTOCPP_MEDIA_HANDLER_CTOCPP_H_
#define CEF_LIBCEF_DLL_CTOCPP_MEDIA_HANDLER_CTOCPP_H_
#pragma once

#if !defined(BUILDING_CEF_SHARED)
#error This file can be included DLL-side only
#endif

#include "include/capi/cef_media_handler_capi.h"
#include "include/cef_media_handler.h"
#include "libcef_dll/ctocpp/ctocpp_ref_counted.h"

// Wrap a C structure with a C++ class.
// This class may be instantiated and accessed DLL-side only.
class CefMediaHandlerCToCpp
    : public CefCToCppRefCounted<CefMediaHandlerCToCpp, CefMediaHandler,
                                 cef_media_handler_t> {
public:
  CefMediaHandlerCToCpp();
  virtual ~CefMediaHandlerCToCpp();

  // CefMediaHandler methods.
  void OnAudioStateChanged(CefRefPtr<CefBrowser> browser,
                           bool audible) override;
  void OnMediaStateChanged(CefRefPtr<CefBrowser> browser, MediaType type,
                           MediaPlayingState state) override;
};

#endif // CEF_LIBCEF_DLL_CTOCPP_MEDIA_HANDLER_CTOCPP_H_
