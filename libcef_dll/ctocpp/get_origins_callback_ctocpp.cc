// Copyright (c) 2023 The Chromium Embedded Framework Authors. All rights
// reserved. Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.
//
// ---------------------------------------------------------------------------
//
// This file was generated by the CEF translator tool. If making changes by
// hand only do so within the body of existing method and function
// implementations. See the translator.README.txt file in the tools directory
// for more information.
//
// $hash=42082bd4962aa5bd8556918888da73635e4b36c5$
//

#include "libcef_dll/ctocpp/get_origins_callback_ctocpp.h"
#include "libcef_dll/shutdown_checker.h"
#include "libcef_dll/transfer_util.h"

// VIRTUAL METHODS - Body may be edited by hand.

NO_SANITIZE("cfi-icall")
void CefGetOriginsCallbackCToCpp::OnOrigins(std::vector<CefString>& origins) {
  shutdown_checker::AssertNotShutdown();

  cef_get_origins_callback_t* _struct = GetStruct();
  if (CEF_MEMBER_MISSING(_struct, on_origins))
    return;

  // AUTO-GENERATED CONTENT - DELETE THIS COMMENT BEFORE MODIFYING

  // Translate param: origins; type: string_vec_byref
  cef_string_list_t originsList = cef_string_list_alloc();
  DCHECK(originsList);
  if (originsList)
    transfer_string_list_contents(origins, originsList);

  // Execute
  _struct->on_origins(_struct, originsList);

  // Restore param:origins; type: string_vec_byref
  if (originsList) {
    origins.clear();
    transfer_string_list_contents(originsList, origins);
    cef_string_list_free(originsList);
  }
}

NO_SANITIZE("cfi-icall")
void CefGetOriginsCallbackCToCpp::OnUsages(std::vector<CefString>& usages) {
  shutdown_checker::AssertNotShutdown();

  cef_get_origins_callback_t* _struct = GetStruct();
  if (CEF_MEMBER_MISSING(_struct, on_usages))
    return;

  // AUTO-GENERATED CONTENT - DELETE THIS COMMENT BEFORE MODIFYING

  // Translate param: usages; type: string_vec_byref
  cef_string_list_t usagesList = cef_string_list_alloc();
  DCHECK(usagesList);
  if (usagesList)
    transfer_string_list_contents(usages, usagesList);

  // Execute
  _struct->on_usages(_struct, usagesList);

  // Restore param:usages; type: string_vec_byref
  if (usagesList) {
    usages.clear();
    transfer_string_list_contents(usagesList, usages);
    cef_string_list_free(usagesList);
  }
}

NO_SANITIZE("cfi-icall")
void CefGetOriginsCallbackCToCpp::OnQuotas(std::vector<CefString>& quotas) {
  shutdown_checker::AssertNotShutdown();

  cef_get_origins_callback_t* _struct = GetStruct();
  if (CEF_MEMBER_MISSING(_struct, on_quotas))
    return;

  // AUTO-GENERATED CONTENT - DELETE THIS COMMENT BEFORE MODIFYING

  // Translate param: quotas; type: string_vec_byref
  cef_string_list_t quotasList = cef_string_list_alloc();
  DCHECK(quotasList);
  if (quotasList)
    transfer_string_list_contents(quotas, quotasList);

  // Execute
  _struct->on_quotas(_struct, quotasList);

  // Restore param:quotas; type: string_vec_byref
  if (quotasList) {
    quotas.clear();
    transfer_string_list_contents(quotasList, quotas);
    cef_string_list_free(quotasList);
  }
}

NO_SANITIZE("cfi-icall") void CefGetOriginsCallbackCToCpp::OnComplete() {
  shutdown_checker::AssertNotShutdown();

  cef_get_origins_callback_t* _struct = GetStruct();
  if (CEF_MEMBER_MISSING(_struct, on_complete))
    return;

  // AUTO-GENERATED CONTENT - DELETE THIS COMMENT BEFORE MODIFYING

  // Execute
  _struct->on_complete(_struct);
}

// CONSTRUCTOR - Do not edit by hand.

CefGetOriginsCallbackCToCpp::CefGetOriginsCallbackCToCpp() {}

// DESTRUCTOR - Do not edit by hand.

CefGetOriginsCallbackCToCpp::~CefGetOriginsCallbackCToCpp() {
  shutdown_checker::AssertNotShutdown();
}

template <>
cef_get_origins_callback_t* CefCToCppRefCounted<
    CefGetOriginsCallbackCToCpp,
    CefGetOriginsCallback,
    cef_get_origins_callback_t>::UnwrapDerived(CefWrapperType type,
                                               CefGetOriginsCallback* c) {
  NOTREACHED() << "Unexpected class type: " << type;
  return nullptr;
}

template <>
CefWrapperType CefCToCppRefCounted<CefGetOriginsCallbackCToCpp,
                                   CefGetOriginsCallback,
                                   cef_get_origins_callback_t>::kWrapperType =
    WT_GET_ORIGINS_CALLBACK;
