// Copyright (c) 2023 The Chromium Embedded Framework Authors. All rights
// reserved. Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.
//
// ---------------------------------------------------------------------------
//
// This file was generated by the CEF translator tool. If making changes by
// hand only do so within the body of existing method and function
// implementations. See the translator.README.txt file in the tools directory
// for more information.
//
// $hash=4f1d80cb3bd00c0c9b94a9e2663f09f13474158a$
//

#include "libcef_dll/ctocpp/java_script_result_callback_ctocpp.h"
#include "libcef_dll/cpptoc/value_cpptoc.h"
#include "libcef_dll/shutdown_checker.h"

// VIRTUAL METHODS - Body may be edited by hand.

NO_SANITIZE("cfi-icall")
void CefJavaScriptResultCallbackCToCpp::OnJavaScriptExeResult(
    CefRefPtr<CefValue> result) {
  shutdown_checker::AssertNotShutdown();

  cef_java_script_result_callback_t* _struct = GetStruct();
  if (CEF_MEMBER_MISSING(_struct, on_java_script_exe_result))
    return;

  // AUTO-GENERATED CONTENT - DELETE THIS COMMENT BEFORE MODIFYING

  // Verify param: result; type: refptr_diff
  DCHECK(result.get());
  if (!result.get())
    return;

  // Execute
  _struct->on_java_script_exe_result(_struct, CefValueCppToC::Wrap(result));
}

// CONSTRUCTOR - Do not edit by hand.

CefJavaScriptResultCallbackCToCpp::CefJavaScriptResultCallbackCToCpp() {}

// DESTRUCTOR - Do not edit by hand.

CefJavaScriptResultCallbackCToCpp::~CefJavaScriptResultCallbackCToCpp() {
  shutdown_checker::AssertNotShutdown();
}

template <>
cef_java_script_result_callback_t*
CefCToCppRefCounted<CefJavaScriptResultCallbackCToCpp,
                    CefJavaScriptResultCallback,
                    cef_java_script_result_callback_t>::
    UnwrapDerived(CefWrapperType type, CefJavaScriptResultCallback* c) {
  NOTREACHED() << "Unexpected class type: " << type;
  return nullptr;
}

template <>
CefWrapperType
    CefCToCppRefCounted<CefJavaScriptResultCallbackCToCpp,
                        CefJavaScriptResultCallback,
                        cef_java_script_result_callback_t>::kWrapperType =
        WT_JAVA_SCRIPT_RESULT_CALLBACK;
