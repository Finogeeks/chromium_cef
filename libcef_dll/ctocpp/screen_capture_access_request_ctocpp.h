// Copyright (c) 2023 The Chromium Embedded Framework Authors. All rights
// reserved. Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.
//
// ---------------------------------------------------------------------------
//
// This file was generated by the CEF translator tool. If making changes by
// hand only do so within the body of existing method and function
// implementations. See the translator.README.txt file in the tools directory
// for more information.
//
// $hash=1d61b206fb2419578bace59d02c629c608847467$
//

#ifndef CEF_LIBCEF_DLL_CTOCPP_SCREEN_CAPTURE_ACCESS_REQUEST_CTOCPP_H_
#define CEF_LIBCEF_DLL_CTOCPP_SCREEN_CAPTURE_ACCESS_REQUEST_CTOCPP_H_
#pragma once

#if !defined(WRAPPING_CEF_SHARED)
#error This file can be included wrapper-side only
#endif

#include "include/capi/cef_permission_request_capi.h"
#include "include/cef_permission_request.h"
#include "libcef_dll/ctocpp/ctocpp_ref_counted.h"

// Wrap a C structure with a C++ class.
// This class may be instantiated and accessed wrapper-side only.
class CefScreenCaptureAccessRequestCToCpp
    : public CefCToCppRefCounted<CefScreenCaptureAccessRequestCToCpp,
                                 CefScreenCaptureAccessRequest,
                                 cef_screen_capture_access_request_t> {
 public:
  CefScreenCaptureAccessRequestCToCpp();
  virtual ~CefScreenCaptureAccessRequestCToCpp();

  // CefScreenCaptureAccessRequest methods.
  CefString Origin() override;
  void SetCaptureMode(int32_t mode) override;
  void SetCaptureSourceId(int32_t sourceId) override;
  void ReportRequestResult(bool allowed) override;
};

#endif  // CEF_LIBCEF_DLL_CTOCPP_SCREEN_CAPTURE_ACCESS_REQUEST_CTOCPP_H_
