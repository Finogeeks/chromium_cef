// Copyright (c) 2023 The Chromium Embedded Framework Authors. All rights
// reserved. Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.
//
// ---------------------------------------------------------------------------
//
// This file was generated by the CEF translator tool. If making changes by
// hand only do so within the body of existing method and function
// implementations. See the translator.README.txt file in the tools directory
// for more information.
//
// $hash=9607b573a87d3440470b9a9fe3de07a1aa92af01$
//

#include "libcef_dll/cpptoc/java_script_result_callback_cpptoc.h"
#include "libcef_dll/ctocpp/value_ctocpp.h"
#include "libcef_dll/shutdown_checker.h"

namespace {

// MEMBER FUNCTIONS - Body may be edited by hand.

void CEF_CALLBACK java_script_result_callback_on_java_script_exe_result(
    struct _cef_java_script_result_callback_t* self,
    struct _cef_value_t* result) {
  shutdown_checker::AssertNotShutdown();

  // AUTO-GENERATED CONTENT - DELETE THIS COMMENT BEFORE MODIFYING

  DCHECK(self);
  if (!self)
    return;
  // Verify param: result; type: refptr_diff
  DCHECK(result);
  if (!result)
    return;

  // Execute
  CefJavaScriptResultCallbackCppToC::Get(self)->OnJavaScriptExeResult(
      CefValueCToCpp::Wrap(result));
}

}  // namespace

// CONSTRUCTOR - Do not edit by hand.

CefJavaScriptResultCallbackCppToC::CefJavaScriptResultCallbackCppToC() {
  GetStruct()->on_java_script_exe_result =
      java_script_result_callback_on_java_script_exe_result;
}

// DESTRUCTOR - Do not edit by hand.

CefJavaScriptResultCallbackCppToC::~CefJavaScriptResultCallbackCppToC() {
  shutdown_checker::AssertNotShutdown();
}

template <>
CefRefPtr<CefJavaScriptResultCallback>
CefCppToCRefCounted<CefJavaScriptResultCallbackCppToC,
                    CefJavaScriptResultCallback,
                    cef_java_script_result_callback_t>::
    UnwrapDerived(CefWrapperType type, cef_java_script_result_callback_t* s) {
  NOTREACHED() << "Unexpected class type: " << type;
  return nullptr;
}

template <>
CefWrapperType
    CefCppToCRefCounted<CefJavaScriptResultCallbackCppToC,
                        CefJavaScriptResultCallback,
                        cef_java_script_result_callback_t>::kWrapperType =
        WT_JAVA_SCRIPT_RESULT_CALLBACK;
