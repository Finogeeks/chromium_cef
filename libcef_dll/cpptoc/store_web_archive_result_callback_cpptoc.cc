// Copyright (c) 2023 The Chromium Embedded Framework Authors. All rights
// reserved. Use of this source code is governed by a BSD-style license that
// can be found in the LICENSE file.
//
// ---------------------------------------------------------------------------
//
// This file was generated by the CEF translator tool. If making changes by
// hand only do so within the body of existing method and function
// implementations. See the translator.README.txt file in the tools directory
// for more information.
//
// $hash=018aea8a22d2cd56b94fdb4afe6cda26e5267e50$
//

#include "libcef_dll/cpptoc/store_web_archive_result_callback_cpptoc.h"
#include "libcef_dll/shutdown_checker.h"

namespace {

// MEMBER FUNCTIONS - Body may be edited by hand.

void CEF_CALLBACK store_web_archive_result_callback_on_store_web_archive_done(
    struct _cef_store_web_archive_result_callback_t* self,
    const cef_string_t* result) {
  shutdown_checker::AssertNotShutdown();

  // AUTO-GENERATED CONTENT - DELETE THIS COMMENT BEFORE MODIFYING

  DCHECK(self);
  if (!self)
    return;
  // Unverified params: result

  // Execute
  CefStoreWebArchiveResultCallbackCppToC::Get(self)->OnStoreWebArchiveDone(
      CefString(result));
}

}  // namespace

// CONSTRUCTOR - Do not edit by hand.

CefStoreWebArchiveResultCallbackCppToC::
    CefStoreWebArchiveResultCallbackCppToC() {
  GetStruct()->on_store_web_archive_done =
      store_web_archive_result_callback_on_store_web_archive_done;
}

// DESTRUCTOR - Do not edit by hand.

CefStoreWebArchiveResultCallbackCppToC::
    ~CefStoreWebArchiveResultCallbackCppToC() {
  shutdown_checker::AssertNotShutdown();
}

template <>
CefRefPtr<CefStoreWebArchiveResultCallback>
CefCppToCRefCounted<CefStoreWebArchiveResultCallbackCppToC,
                    CefStoreWebArchiveResultCallback,
                    cef_store_web_archive_result_callback_t>::
    UnwrapDerived(CefWrapperType type,
                  cef_store_web_archive_result_callback_t* s) {
  NOTREACHED() << "Unexpected class type: " << type;
  return nullptr;
}

template <>
CefWrapperType
    CefCppToCRefCounted<CefStoreWebArchiveResultCallbackCppToC,
                        CefStoreWebArchiveResultCallback,
                        cef_store_web_archive_result_callback_t>::kWrapperType =
        WT_STORE_WEB_ARCHIVE_RESULT_CALLBACK;
