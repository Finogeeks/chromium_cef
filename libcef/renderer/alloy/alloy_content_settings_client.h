// Copyright (c) 2022 Huawei Device Co., Ltd. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef CEF_LIBCEF_RENDERER_ALLOY_ALLOY_CONTENT_SETTINGS_CLIENT_H_
#define CEF_LIBCEF_RENDERER_ALLOY_ALLOY_CONTENT_SETTINGS_CLIENT_H_

#include "content/public/renderer/render_frame_observer.h"
#include "third_party/blink/public/platform/web_content_settings_client.h"

// NWeb implementation of blink::WebContentSettingsClient.
class AlloyContentSettingsClient : public content::RenderFrameObserver,
                                   public blink::WebContentSettingsClient {
 public:
  AlloyContentSettingsClient(const AlloyContentSettingsClient&) = delete;
  AlloyContentSettingsClient& operator=(const AlloyContentSettingsClient&) =
      delete;

  explicit AlloyContentSettingsClient(content::RenderFrame* render_view);

 private:
  ~AlloyContentSettingsClient() override;

  // content::RenderFrameObserver implementation.
  void OnDestruct() override;

  bool ShouldAutoupgradeMixedContent() override;
};

#endif  // CEF_LIBCEF_RENDERER_ALLOY_ALLOY_CONTENT_SETTINGS_CLIENT_H_
