// Copyright (c) 2022 Huawei Device Co., Ltd. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef CEF_LIBCEF_BROWSER_NET_SERVICE_NET_HELPERS_H_
#define CEF_LIBCEF_BROWSER_NET_SERVICE_NET_HELPERS_H_

#include <string>

#include "build/build_config.h"
#include "net/dns/public/secure_dns_mode.h"

class GURL;

namespace net_service {

#define NETHELPERS_EXPORT __attribute__((visibility("default")))

struct NetHelperSetting {
  bool file_access;
  bool block_network;
  int cache_mode;
};

class NETHELPERS_EXPORT NetHelpers {
 public:
  static bool ShouldBlockContentUrls();
  static bool ShouldBlockFileUrls(struct NetHelperSetting setting);
  static bool IsAllowAcceptCookies();
  static bool IsThirdPartyCookieAllowed();

#if BUILDFLAG(IS_OHOS)
  static net::SecureDnsMode DnsOverHttpMode();
  static std::string DnsOverHttpServerConfig();
  static bool HasValidDnsOverHttpConfig();
#endif

  static bool allow_content_access;
  static bool allow_file_access;
  static bool is_network_blocked;
  static bool accept_cookies;
  static bool third_party_cookies;
  static int cache_mode;
  static int connection_timeout;

#if BUILDFLAG(IS_OHOS)
  static int doh_mode;
  static std::string doh_config;
#endif
};

bool IsSpecialFileUrl(const GURL& url);

// Update request's |load_flags| based on the settings.
int UpdateLoadFlags(int load_flags, struct NetHelperSetting setting);

// Returns true if the given URL should be aborted with
// net::ERR_ACCESS_DENIED.
bool IsURLBlocked(const GURL& url, struct NetHelperSetting setting);

}  // namespace net_service

#endif  // CEF_LIBCEF_BROWSER_NET_SERVICE_NET_HELPERS_H_
