// Copyright (c) 2022 Huawei Device Co., Ltd. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef OH_JAVASCRIPT_INJECTOR_H
#define OH_JAVASCRIPT_INJECTOR_H

#include "content/public/browser/web_contents_user_data.h"
#include "include/cef_client.h"
#include "libcef/browser/browser_info.h"

namespace NWEB {
class OhGinJavascriptBridgeDispatcherHost;

class OhJavascriptInjector
    : public content::WebContentsUserData<OhJavascriptInjector> {
 public:
  OhJavascriptInjector(content::WebContents* web_contents,
                       CefRefPtr<CefClient> client);

  OhJavascriptInjector(const OhJavascriptInjector&) = delete;
  OhJavascriptInjector& operator=(const OhJavascriptInjector&) = delete;
  ~OhJavascriptInjector();

  void AddInterface(const std::string& object_name,
                    const std::vector<std::string> method_list,
                    const int32_t object_id);
  void RemoveInterface(const std::string& object_name,
                       const std::vector<std::string> method_list);
  void DoCallH5Function(int32_t routing_id,
                        int32_t h5_object_id,
                        const std::string& h5_method_name,
                        const std::vector<CefRefPtr<CefValue>>& args);

 private:
  friend class content::WebContentsUserData<OhJavascriptInjector>;

  scoped_refptr<OhGinJavascriptBridgeDispatcherHost>
      javascript_bridge_dispatcher_host_;
  WEB_CONTENTS_USER_DATA_KEY_DECL();
};
}  // namespace NWEB
#endif