// Copyright 2019 The Chromium Embedded Framework Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef CEF_LIBCEF_BROWSER_OSR_HOST_DISPLAY_CLIENT_OSR_H_
#define CEF_LIBCEF_BROWSER_OSR_HOST_DISPLAY_CLIENT_OSR_H_

#include <memory>

#include "base/callback.h"
#include "base/memory/shared_memory_mapping.h"
#include "components/viz/host/host_display_client.h"
#include "libcef/browser/alloy/alloy_browser_host_impl.h"
#include "ui/gfx/native_widget_types.h"

class CefLayeredWindowUpdaterOSR;
class CefRenderWidgetHostViewOSR;

class CefHostDisplayClientOSR : public viz::HostDisplayClient {
 public:
  CefHostDisplayClientOSR(CefRenderWidgetHostViewOSR* const view,
                          gfx::AcceleratedWidget widget);

  CefHostDisplayClientOSR(const CefHostDisplayClientOSR&) = delete;
  CefHostDisplayClientOSR& operator=(const CefHostDisplayClientOSR&) = delete;

  ~CefHostDisplayClientOSR() override;

  void SetActive(bool active);
  const void* GetPixelMemory() const;
  gfx::Size GetPixelSize() const;

 private:
  // mojom::DisplayClient implementation.
  void UseProxyOutputDevice(UseProxyOutputDeviceCallback callback) override;

#ifdef DISABLE_GPU
  void CreateLayeredWindowUpdater(
      mojo::PendingReceiver<viz::mojom::LayeredWindowUpdater> receiver)
      override;
#endif

#if BUILDFLAG(IS_LINUX)
  void DidCompleteSwapWithNewSize(const gfx::Size& size) override;
#endif

#if BUILDFLAG(IS_OHOS)
  void DidCompleteSwapWithNewSizeOHOS(const gfx::Size& size) override;
  CefRefPtr<AlloyBrowserHostImpl> browser_impl_;
#endif

  CefRenderWidgetHostViewOSR* const view_;
  std::unique_ptr<CefLayeredWindowUpdaterOSR> layered_window_updater_;
  bool active_ = false;
};

#endif  // CEF_LIBCEF_BROWSER_OSR_HOST_DISPLAY_CLIENT_OSR_H_
