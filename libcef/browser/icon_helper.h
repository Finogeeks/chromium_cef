// Copyright (c) 2022 Huawei Device Co., Ltd.
// Copyright 2018 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef CEF_LIBCEF_BROWSER_ICON_HELPER_H_
#define CEF_LIBCEF_BROWSER_ICON_HELPER_H_
#pragma once

#include <unordered_set>
#include <vector>

#include "include/cef_base.h"
#include "libcef/browser/thread_util.h"

#include "content/public/browser/navigation_handle.h"
#include "third_party/blink/public/mojom/favicon/favicon_url.mojom-forward.h"
#include "third_party/skia/include/core/SkBitmap.h"

namespace gfx {
class Size;
}  // namespace gfx

namespace content {
class RenderFrameHost;
class WebContents;
}  // namespace content

class CefBrowser;
class CefDisplayHandler;
class GURL;

class IconHelper : public virtual CefBaseRefCounted {
 public:
  IconHelper() = default;
  ~IconHelper() { web_contents_ = nullptr; }

  IconHelper(const IconHelper&) = delete;
  IconHelper& operator=(const IconHelper&) = delete;

#if BUILDFLAG(IS_OHOS)
  void SetMainFrameDocumentOnLoadCompleted(bool complete);
  void SetLastPageUrl(const GURL& url);
#endif
  void SetDisplayHandler(const CefRefPtr<CefDisplayHandler>& handler);
  void SetBrowser(const CefRefPtr<CefBrowser>& browser);
  void SetWebContents(content::WebContents* new_contents);
  void OnUpdateFaviconURL(
      content::RenderFrameHost* render_frame_host,
      const std::vector<blink::mojom::FaviconURLPtr>& candidates);
  void DownloadFavicon(const blink::mojom::FaviconURLPtr& candidate);
  void DownloadFaviconCallback(
      int id,
      int http_status_code,
      const GURL& image_url,
      const std::vector<SkBitmap>& bitmaps,
      const std::vector<gfx::Size>& original_bitmap_sizes);
  void OnReceivedIcon(const void* data,
                      size_t width,
                      size_t height,
                      cef_color_type_t color_type,
                      cef_alpha_type_t alpha_type);

  void OnReceivedIconUrl(const CefString& image_url,
                         const void* data,
                         size_t width,
                         size_t height,
                         cef_color_type_t color_type,
                         cef_alpha_type_t alpha_type);
  void ClearFailedFaviconUrlSets(content::NavigationHandle* navigation_handle);

 private:
  void InsertFailedFaviconUrl(const GURL& icon_url);
  bool CheckFailedFaviconUrl(const GURL& icon_url);
#if BUILDFLAG(IS_OHOS)
  bool ShouldAbort();

  bool document_on_load_completed_ = false;
  GURL last_page_url_;
#endif
  content::WebContents* web_contents_ = nullptr;
  CefRefPtr<CefDisplayHandler> handler_ = nullptr;
  CefRefPtr<CefBrowser> browser_ = nullptr;
  SkBitmap bitmap_;
  std::unordered_set<size_t> failed_favicon_urls_set_;

  IMPLEMENT_REFCOUNTING_DELETE_ON_UIT(IconHelper);
};

#endif  // CEF_LIBCEF_BROWSER_ICON_HELPER_H_
