// Copyright (c) 2022 Huawei Device Co., Ltd. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "libcef/browser/permission/alloy_permission_manager.h"

#include <memory>
#include <string>

#include "base/callback.h"
#include "base/logging.h"
#include "components/permissions/permission_util.h"
#include "content/public/browser/permission_controller.h"
#include "content/public/browser/permission_type.h"
#include "content/public/browser/render_frame_host.h"
#include "content/public/browser/render_process_host.h"
#include "content/public/browser/web_contents.h"

using blink::mojom::PermissionStatus;
using content::PermissionType;

using RequestPermissionsCallback =
    base::OnceCallback<void(const std::vector<PermissionStatus>&)>;

namespace {

void PermissionRequestResponseCallbackWrapper(
    base::OnceCallback<void(PermissionStatus)> callback,
    const std::vector<PermissionStatus>& vector) {
  DCHECK_EQ(vector.size(), 1ul);
  std::move(callback).Run(vector.at(0));
}

}  // namespace

class AlloyPermissionManager::UnhandledRequest {
 public:
  UnhandledRequest(const std::vector<PermissionType> permissions,
                   content::RenderFrameHost* render_frame_host,
                   GURL requesting_origin,
                   RequestPermissionsCallback callback)
      : permissions_(permissions),
        render_frame_host_(render_frame_host),
        requesting_origin_(requesting_origin),
        callback_(std::move(callback)),
        results_(permissions.size(), PermissionStatus::DENIED),
        cancelled_(false) {
    for (size_t i = 0; i < permissions.size(); ++i)
      permission_index_map_.insert(std::make_pair(permissions[i], i));
  }

  ~UnhandledRequest() = default;

  bool HasPermissionType(PermissionType type) {
    return permission_index_map_.find(type) != permission_index_map_.end();
  }

  bool IsCompleted(PermissionType type) const {
    return resolved_permissions_.count(type) != 0;
  }

  bool IsCompleted() const {
    return results_.size() == resolved_permissions_.size();
  }

  void SetPermissionStatus(PermissionType type, PermissionStatus status) {
    auto result = permission_index_map_.find(type);
    if (result == permission_index_map_.end()) {
      NOTREACHED();
      return;
    }
    DCHECK(!IsCompleted());
    results_[result->second] = status;
    resolved_permissions_.insert(type);
  }

  PermissionStatus GetPermissionStatus(PermissionType type) {
    auto result = permission_index_map_.find(type);
    if (result == permission_index_map_.end()) {
      NOTREACHED();
      return PermissionStatus::DENIED;
    }
    return results_[result->second];
  }

  void Cancel() { cancelled_ = true; }

  bool IsCancelled() const { return cancelled_; }

  std::vector<PermissionType> permissions_;
  content::RenderFrameHost* render_frame_host_;
  GURL requesting_origin_;
  RequestPermissionsCallback callback_;
  std::vector<PermissionStatus> results_;

 private:
  std::map<PermissionType, size_t> permission_index_map_;
  std::set<PermissionType> resolved_permissions_;
  bool cancelled_;
};

AlloyPermissionManager::AlloyPermissionManager() {}

AlloyPermissionManager::~AlloyPermissionManager() {
  AbortPermissionRequests();
}

void AlloyPermissionManager::RequestPermission(
    content::PermissionType permission,
    content::RenderFrameHost* render_frame_host,
    const GURL& requesting_origin,
    bool user_gesture,
    base::OnceCallback<void(blink::mojom::PermissionStatus)> callback) {
  return RequestPermissions(
      std::vector<PermissionType>(1, permission), render_frame_host,
      requesting_origin, user_gesture,
      base::BindOnce(&PermissionRequestResponseCallbackWrapper,
                     std::move(callback)));
}

void AlloyPermissionManager::RequestPermissions(
    const std::vector<content::PermissionType>& permissions,
    content::RenderFrameHost* render_frame_host,
    const GURL& requesting_origin,
    bool user_gesture,
    base::OnceCallback<void(const std::vector<blink::mojom::PermissionStatus>&)>
        callback) {
  if (permissions.empty()) {
    std::move(callback).Run(std::vector<PermissionStatus>());
    return;
  }

  auto pending_request = std::make_unique<UnhandledRequest>(
      permissions, render_frame_host, requesting_origin, std::move(callback));
  std::vector<bool> should_delegate_requests =
      std::vector<bool>(permissions.size(), true);
  for (size_t i = 0; i < permissions.size(); ++i) {
    for (UnhandledRequestsMap::Iterator<UnhandledRequest> it(
             &unhandled_requests_);
         !it.IsAtEnd(); it.Advance()) {
      if (!it.GetCurrentValue()->HasPermissionType(permissions[i]) ||
          it.GetCurrentValue()->requesting_origin_ != requesting_origin) {
        continue;
      }
      if (it.GetCurrentValue()->IsCompleted(permissions[i])) {
        pending_request->SetPermissionStatus(
            permissions[i],
            it.GetCurrentValue()->GetPermissionStatus(permissions[i]));
      }
      should_delegate_requests[i] = false;
      break;
    }
  }

  // Keep copy of pointer for performing further operations after ownership is
  // transferred to unhandled_requests_
  UnhandledRequest* pending_request_raw = pending_request.get();
  const int request_id = unhandled_requests_.Add(std::move(pending_request));

  CefRefPtr<CefBrowserHostBase> browser =
      CefBrowserHostBase::GetBrowserForHost(render_frame_host);

  for (size_t i = 0; i < permissions.size(); ++i) {
    if (should_delegate_requests[i])
      RequestPermissionByType(permissions[i], browser, pending_request_raw,
                              request_id);
  }

  // If delegate resolve the permission synchronously, all requests could be
  // already resolved here.
  if (!unhandled_requests_.Lookup(request_id))
    return;

  // If requests are resolved without calling delegate functions, e.g.
  // PermissionType::MIDI is permitted within the previous for-loop, all
  // requests could be already resolved, but still in the |unhandled_requests_|
  // without invoking the callback.
  if (pending_request_raw->IsCompleted()) {
    std::vector<PermissionStatus> results = pending_request_raw->results_;
    RequestPermissionsCallback completed_callback =
        std::move(pending_request_raw->callback_);
    unhandled_requests_.Remove(request_id);
    std::move(completed_callback).Run(results);
    return;
  }

  return;
}

void AlloyPermissionManager::RequestPermissionByType(
    const content::PermissionType permission_type,
    CefRefPtr<CefBrowserHostBase> browser,
    UnhandledRequest* pending_request_raw,
    const int request_id) {
  switch (permission_type) {
    case PermissionType::GEOLOCATION:
      if (browser)
        browser->AskGeolocationPermission(
            pending_request_raw->requesting_origin_.spec(),
            base::BindRepeating(
                &AlloyPermissionManager::OnRequestResponseCallBack,
                weak_ptr_factory_.GetWeakPtr(), request_id, permission_type));
      break;
    case PermissionType::PROTECTED_MEDIA_IDENTIFIER:
      browser->AskProtectedMediaIdentifierPermission(
          pending_request_raw->requesting_origin_.spec(),
          base::BindRepeating(
              &AlloyPermissionManager::OnRequestResponseCallBack,
              weak_ptr_factory_.GetWeakPtr(), request_id, permission_type));
      break;
    case PermissionType::MIDI_SYSEX:
      browser->AskMIDISysexPermission(
          pending_request_raw->requesting_origin_.spec(),
          base::BindRepeating(
              &AlloyPermissionManager::OnRequestResponseCallBack,
              weak_ptr_factory_.GetWeakPtr(), request_id, permission_type));
      break;
    case PermissionType::AUDIO_CAPTURE:
    case PermissionType::VIDEO_CAPTURE:
    case PermissionType::NOTIFICATIONS:
    case PermissionType::DURABLE_STORAGE:
    case PermissionType::BACKGROUND_SYNC:
    case PermissionType::ACCESSIBILITY_EVENTS:
    case PermissionType::PAYMENT_HANDLER:
    case PermissionType::BACKGROUND_FETCH:
    case PermissionType::IDLE_DETECTION:
    case PermissionType::PERIODIC_BACKGROUND_SYNC:
    case PermissionType::NFC:
    case PermissionType::VR:
    case PermissionType::AR:
    case PermissionType::STORAGE_ACCESS_GRANT:
    case PermissionType::CAMERA_PAN_TILT_ZOOM:
    case PermissionType::WINDOW_PLACEMENT:
    case PermissionType::FONT_ACCESS:
    case PermissionType::DISPLAY_CAPTURE:
    case PermissionType::MIDI:
    case PermissionType::SENSORS:
    case PermissionType::WAKE_LOCK_SYSTEM:
    default:
      NOTREACHED() << "Invalid PermissionType.";
      pending_request_raw->SetPermissionStatus(permission_type,
                                               PermissionStatus::DENIED);
      break;
    case PermissionType::CLIPBOARD_READ_WRITE:
    case PermissionType::CLIPBOARD_SANITIZED_WRITE:
    case PermissionType::WAKE_LOCK_SCREEN:
      pending_request_raw->SetPermissionStatus(permission_type,
                                               PermissionStatus::GRANTED);
      break;
  }
}

void AlloyPermissionManager::OnRequestResponseCallBack(
    const base::WeakPtr<AlloyPermissionManager>& manager,
    int request_id,
    PermissionType permission,
    bool allowed) {
  // All delegate functions should be cancelled when the manager runs
  // destructor. Therefore |manager| should be always valid here.
  DCHECK(manager);
  PermissionStatus status =
      allowed ? PermissionStatus::GRANTED : PermissionStatus::DENIED;
  UnhandledRequest* pending_request =
      manager->unhandled_requests_.Lookup(request_id);
  std::vector<int> complete_request_ids;
  std::vector<
      std::pair<RequestPermissionsCallback, std::vector<PermissionStatus>>>
      complete_request_pairs;
  for (UnhandledRequestsMap::Iterator<UnhandledRequest> it(
           &manager->unhandled_requests_);
       !it.IsAtEnd(); it.Advance()) {
    if (!it.GetCurrentValue()->HasPermissionType(permission) ||
        it.GetCurrentValue()->requesting_origin_ !=
            pending_request->requesting_origin_) {
      continue;
    }
    it.GetCurrentValue()->SetPermissionStatus(permission, status);
    if (it.GetCurrentValue()->IsCompleted()) {
      complete_request_ids.push_back(it.GetCurrentKey());
      if (!it.GetCurrentValue()->IsCancelled()) {
        complete_request_pairs.emplace_back(
            std::move(it.GetCurrentValue()->callback_),
            std::move(it.GetCurrentValue()->results_));
      }
    }
  }
  for (auto id : complete_request_ids)
    manager->unhandled_requests_.Remove(id);
  for (auto& pair : complete_request_pairs)
    std::move(pair.first).Run(pair.second);
}

PermissionStatus AlloyPermissionManager::GetPermissionStatus(
    PermissionType permission,
    const GURL& requesting_origin,
    const GURL& embedding_origin) {
  if (permission == PermissionType::CLIPBOARD_READ_WRITE ||
      permission == PermissionType::CLIPBOARD_SANITIZED_WRITE) {
    return PermissionStatus::GRANTED;
  }
  return PermissionStatus::DENIED;
}

PermissionStatus AlloyPermissionManager::GetPermissionStatusForFrame(
    PermissionType permission,
    content::RenderFrameHost* render_frame_host,
    const GURL& requesting_origin) {
  return GetPermissionStatus(
      permission, requesting_origin,
      permissions::PermissionUtil::GetLastCommittedOriginAsURL(
          render_frame_host->GetMainFrame()));
}

void AlloyPermissionManager::ResetPermission(PermissionType permission,
                                             const GURL& requesting_origin,
                                             const GURL& embedding_origin) {}

AlloyPermissionManager::SubscriptionId
AlloyPermissionManager::SubscribePermissionStatusChange(
    content::PermissionType permission,
    content::RenderFrameHost* render_frame_host,
    const GURL& requesting_origin,
    base::RepeatingCallback<void(PermissionStatus)> callback) {
  return SubscriptionId();
}

void AlloyPermissionManager::UnsubscribePermissionStatusChange(
    SubscriptionId subscription_id) {}

void AlloyPermissionManager::AbortPermissionRequests() {
  std::vector<int> request_ids;
  for (UnhandledRequestsMap::Iterator<UnhandledRequest> it(
           &unhandled_requests_);
       !it.IsAtEnd(); it.Advance()) {
    request_ids.push_back(it.GetCurrentKey());
  }
  for (auto request_id : request_ids)
    AbortPermissionRequest(request_id);
  DCHECK(unhandled_requests_.IsEmpty());
}

void AlloyPermissionManager::AbortPermissionRequest(int request_id) {
  UnhandledRequest* pending_request = unhandled_requests_.Lookup(request_id);
  if (pending_request == nullptr || pending_request->IsCancelled())
    return;
  pending_request->Cancel();

  const GURL& requesting_origin = pending_request->requesting_origin_;

  CefRefPtr<CefBrowserHostBase> browser = CefBrowserHostBase::GetBrowserForHost(
      pending_request->render_frame_host_);

  for (auto permission : pending_request->permissions_) {
    // If the permission was already resolved, we do not need to cancel it.
    if (pending_request->IsCompleted(permission))
      continue;

    // If another pending_request waits for the same permission being resolved,
    // we should not cancel the delegate's request.
    bool should_not_cancel_ = false;
    for (UnhandledRequestsMap::Iterator<UnhandledRequest> it(
             &unhandled_requests_);
         !it.IsAtEnd(); it.Advance()) {
      if (it.GetCurrentValue() != pending_request &&
          it.GetCurrentValue()->HasPermissionType(permission) &&
          it.GetCurrentValue()->requesting_origin_ == requesting_origin &&
          !it.GetCurrentValue()->IsCompleted(permission)) {
        should_not_cancel_ = true;
        break;
      }
    }
    if (should_not_cancel_)
      continue;
    AbortPermissionRequestByType(permission, browser, requesting_origin);
    pending_request->SetPermissionStatus(permission, PermissionStatus::DENIED);
  }

  // If there are still active requests, we should not remove request_id here,
  // but just do not invoke a relevant callback when the request is resolved in
  // OnRequestResponseCallBack().
  if (pending_request->IsCompleted())
    unhandled_requests_.Remove(request_id);
}

void AlloyPermissionManager::AbortPermissionRequestByType(
    const content::PermissionType permission_type,
    CefRefPtr<CefBrowserHostBase> browser,
    const GURL& requesting_origin) {
  switch (permission_type) {
    case PermissionType::GEOLOCATION:
      if (browser)
        browser->AbortAskGeolocationPermission(requesting_origin.spec());
      break;
    case PermissionType::PROTECTED_MEDIA_IDENTIFIER:
      if (browser)
        browser->AbortAskProtectedMediaIdentifierPermission(
            requesting_origin.spec());
      break;
    case PermissionType::MIDI_SYSEX:
      if (browser)
        browser->AbortAskMIDISysexPermission(requesting_origin.spec());
      break;
    case PermissionType::NOTIFICATIONS:
    case PermissionType::DURABLE_STORAGE:
    case PermissionType::AUDIO_CAPTURE:
    case PermissionType::VIDEO_CAPTURE:
    case PermissionType::BACKGROUND_SYNC:
    case PermissionType::ACCESSIBILITY_EVENTS:
    case PermissionType::CLIPBOARD_READ_WRITE:
    case PermissionType::CLIPBOARD_SANITIZED_WRITE:
    case PermissionType::PAYMENT_HANDLER:
    case PermissionType::BACKGROUND_FETCH:
    case PermissionType::IDLE_DETECTION:
    case PermissionType::PERIODIC_BACKGROUND_SYNC:
    case PermissionType::NFC:
    case PermissionType::VR:
    case PermissionType::AR:
    case PermissionType::STORAGE_ACCESS_GRANT:
    case PermissionType::CAMERA_PAN_TILT_ZOOM:
    case PermissionType::WINDOW_PLACEMENT:
    case PermissionType::FONT_ACCESS:
    case PermissionType::DISPLAY_CAPTURE:
    case PermissionType::MIDI:
    case PermissionType::SENSORS:
    case PermissionType::WAKE_LOCK_SCREEN:
    case PermissionType::WAKE_LOCK_SYSTEM:
    default:
      NOTREACHED() << "Invalid PermissionType.";
      break;
  }
}
