// Copyright 2013 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "libcef/browser/devtools/devtools_manager_delegate.h"

#include <pwd.h>
#include <stdint.h>

#include <vector>

#include "base/atomicops.h"
#include "base/bind.h"
#include "base/command_line.h"
#include "base/files/file_path.h"
#include "base/memory/ptr_util.h"
#include "base/strings/string_number_conversions.h"
#include "base/strings/stringprintf.h"
#include "base/strings/utf_string_conversions.h"
#include "build/build_config.h"
#include "cef/grit/cef_resources.h"
#include "content/public/browser/android/devtools_auth.h"
#include "content/public/browser/browser_context.h"
#include "content/public/browser/devtools_agent_host.h"
#include "content/public/browser/devtools_frontend_host.h"
#include "content/public/browser/devtools_socket_factory.h"
#include "content/public/browser/favicon_status.h"
#include "content/public/browser/navigation_entry.h"
#include "content/public/browser/render_view_host.h"
#include "content/public/browser/web_contents.h"
#include "content/public/common/content_switches.h"
#include "content/public/common/url_constants.h"
#include "content/public/common/user_agent.h"
#include "net/base/net_errors.h"
#include "net/log/net_log_source.h"
#include "net/socket/tcp_server_socket.h"
#include "net/socket/unix_domain_server_socket_posix.h"
#include "ui/base/resource/resource_bundle.h"

namespace content {

bool CanUserConnectToDevTools(
    const net::UnixDomainServerSocket::Credentials& credentials) {
  struct passwd* creds = getpwuid(credentials.user_id);
  if (!creds || !creds->pw_name) {
    LOG(WARNING) << "DevTools: can't obtain creds for uid "
                 << credentials.user_id;
    return false;
  }
  if (credentials.group_id == credentials.user_id &&
      (strcmp("root", creds->pw_name) == 0 ||   // For rooted devices
       strcmp("shell", creds->pw_name) == 0 ||  // For non-rooted devices

       // From processes signed with the same key
       credentials.user_id == getuid())) {
    return true;
  }
  LOG(WARNING) << "DevTools: connection attempt from " << creds->pw_name;
  return false;
}

}  // namespace content

namespace {

const char kSocketNameFormat[] = "webview_devtools_remote_%d";
const char kTetheringSocketName[] = "webview_devtools_tethering_%d_%d";

const int kBackLog = 10;

// Factory for UnixDomainServerSocket.
class UnixDomainServerSocketFactory : public content::DevToolsSocketFactory {
 public:
  explicit UnixDomainServerSocketFactory(const std::string& socket_name)
      : socket_name_(socket_name), last_tethering_socket_(0) {}

  UnixDomainServerSocketFactory(const UnixDomainServerSocketFactory&) = delete;
  UnixDomainServerSocketFactory& operator=(
      const UnixDomainServerSocketFactory&) = delete;

 private:
  // content::DevToolsAgentHost::ServerSocketFactory.
  std::unique_ptr<net::ServerSocket> CreateForHttpServer() override {
    std::unique_ptr<net::UnixDomainServerSocket> socket(
        new net::UnixDomainServerSocket(
            base::BindRepeating(&content::CanUserConnectToDevTools),
            true /* use_abstract_namespace */));
    if (socket->BindAndListen(socket_name_, kBackLog) != net::OK)
      return nullptr;
    return socket;
  }

  std::unique_ptr<net::ServerSocket> CreateForTethering(
      std::string* name) override {
    *name = base::StringPrintf(kTetheringSocketName, getpid(),
                               ++last_tethering_socket_);
    std::unique_ptr<net::UnixDomainServerSocket> socket(
        new net::UnixDomainServerSocket(
            base::BindRepeating(&content::CanUserConnectToDevTools),
            true /* use_abstract_namespace */));
    if (socket->BindAndListen(*name, kBackLog) != net::OK)
      return nullptr;

    return socket;
  }

  std::string socket_name_;
  int last_tethering_socket_;
};

#if BUILDFLAG(IS_OHOS)
class TCPServerSocketFactory : public content::DevToolsSocketFactory {
 public:
  TCPServerSocketFactory(const std::string& address, uint16_t port)
      : address_(address), port_(port) {}

  TCPServerSocketFactory(const TCPServerSocketFactory&) = delete;
  TCPServerSocketFactory& operator=(const TCPServerSocketFactory&) = delete;

 private:
  // content::DevToolsSocketFactory.
  std::unique_ptr<net::ServerSocket> CreateForHttpServer() override {
    std::unique_ptr<net::ServerSocket> socket(
        new net::TCPServerSocket(nullptr, net::NetLogSource()));
    if (socket->ListenWithAddressAndPort(address_, port_, kBackLog) != net::OK)
      return std::unique_ptr<net::ServerSocket>();
    return socket;
  }

  std::unique_ptr<net::ServerSocket> CreateForTethering(
      std::string* out_name) override {
    return nullptr;
  }

  std::string address_;
  uint16_t port_;
};

std::unique_ptr<content::DevToolsSocketFactory> CreateSocketFactory() {
#if BUILDFLAG(IS_OHOS)
  LOG(INFO) << "Domain Socket Entry.";
  return std::make_unique<UnixDomainServerSocketFactory>(
      base::StringPrintf(kSocketNameFormat, getpid()));
#else
  const base::CommandLine& command_line =
      *base::CommandLine::ForCurrentProcess();
  // See if the user specified a port on the command line. Specifying 0 would
  // result in the selection of an ephemeral port but that doesn't make sense
  // for CEF where the URL is otherwise undiscoverable. Also, don't allow
  // binding of ports between 0 and 1024 exclusive because they're normally
  // restricted to root on Posix-based systems.
  uint16_t port = 0;
  if (command_line.HasSwitch(switches::kRemoteDebuggingPort)) {
    int temp_port;
    std::string port_str =
        command_line.GetSwitchValueASCII(switches::kRemoteDebuggingPort);
    if (base::StringToInt(port_str, &temp_port) && temp_port >= 1024 &&
        temp_port < 65535) {
      port = static_cast<uint16_t>(temp_port);
    } else {
      DLOG(WARNING) << "Invalid http debugger port number " << temp_port;
    }
  }
  if (port == 0)
    return nullptr;
  return std::unique_ptr<content::DevToolsSocketFactory>(
      new TCPServerSocketFactory("127.0.0.1", port));
#endif
}
#endif

}  //  namespace

// CefDevToolsManagerDelegate ----------------------------------------------

// static
void CefDevToolsManagerDelegate::StartHttpHandler(
    content::BrowserContext* browser_context) {
  std::unique_ptr<content::DevToolsSocketFactory> socket_factory =
      CreateSocketFactory();
  if (!socket_factory)
    return;

  LOG(INFO) << "start remote debugging server";
  if (browser_context == nullptr) {
    content::DevToolsAgentHost::StartRemoteDebuggingServer(
        std::move(socket_factory), base::FilePath(), base::FilePath());
  } else {
    content::DevToolsAgentHost::StartRemoteDebuggingServer(
        std::move(socket_factory), browser_context->GetPath(),
        base::FilePath());
  }

  const base::CommandLine& command_line =
      *base::CommandLine::ForCurrentProcess();
  if (command_line.HasSwitch(switches::kRemoteDebuggingPipe)) {
    content::DevToolsAgentHost::StartRemoteDebuggingPipeHandler(
        base::OnceClosure());
  }
}

// static
void CefDevToolsManagerDelegate::StopHttpHandler() {
  // This is a no-op if the server was never started.
  LOG(INFO) << "stop remote debugging server";
  content::DevToolsAgentHost::StopRemoteDebuggingServer();
}

CefDevToolsManagerDelegate::CefDevToolsManagerDelegate() {}

CefDevToolsManagerDelegate::~CefDevToolsManagerDelegate() {}

scoped_refptr<content::DevToolsAgentHost>
CefDevToolsManagerDelegate::CreateNewTarget(const GURL& url) {
  // This is reached when the user selects "Open link in new tab" from the
  // DevTools interface.
  // TODO(cef): Consider exposing new API to support this.
  return nullptr;
}

std::string CefDevToolsManagerDelegate::GetDiscoveryPageHTML() {
  return ui::ResourceBundle::GetSharedInstance().LoadDataResourceString(
      IDR_CEF_DEVTOOLS_DISCOVERY_PAGE);
}

bool CefDevToolsManagerDelegate::HasBundledFrontendResources() {
  return true;
}
