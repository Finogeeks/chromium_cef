// Copyright (c) 2022 Huawei Device Co., Ltd. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "libcef/browser/predictors/loading_predictor_factory.h"

#include "components/keyed_service/content/browser_context_dependency_manager.h"
#include "components/keyed_service/content/browser_context_keyed_service_factory.h"
#include "libcef/browser/predictors/loading_predictor.h"

namespace ohos_predictors {

// static
LoadingPredictor* LoadingPredictorFactory::GetForBrowserContext(
    content::BrowserContext* context) {
  return static_cast<LoadingPredictor*>(
      GetInstance()->GetServiceForBrowserContext(context, true));
}

// static
LoadingPredictorFactory* LoadingPredictorFactory::GetInstance() {
  return base::Singleton<LoadingPredictorFactory>::get();
}

LoadingPredictorFactory::LoadingPredictorFactory()
    : BrowserContextKeyedServiceFactory(
          "LoadingPredictor",
          BrowserContextDependencyManager::GetInstance()) {}

LoadingPredictorFactory::~LoadingPredictorFactory() {}

KeyedService* LoadingPredictorFactory::BuildServiceInstanceFor(
    content::BrowserContext* context) const {
  if (!IsLoadingPredictorEnabled(context))
    return nullptr;

  return new LoadingPredictor(LoadingPredictorConfig(), context);
}

}  // namespace ohos_predictors
