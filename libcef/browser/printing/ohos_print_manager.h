// Copyright (c) 2023 Huawei Device Co., Ltd. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.d
#ifndef OHOS_PRINT_MANAGER_H_
#define OHOS_PRINT_MANAGER_H_

#include <memory>
#include <vector>

#include "components/printing/browser/print_manager.h"
#include "components/printing/common/print.mojom-forward.h"
#include "content/public/browser/web_contents_user_data.h"
#include "include/cef_base.h"
#include "ohos_adapter_helper.h"
#include "printing/print_settings.h"

namespace printing {

struct PrintAttrs {
  std::string jobId;
  OHOS::NWeb::PrintAttributesAdapter attrs;
  uint32_t fd;
  std::function<void(std::string, uint32_t)> writeResultCallback;
};

class OhosPrintManager : public printing::PrintManager,
                         public content::WebContentsUserData<OhosPrintManager> {
 public:
  OhosPrintManager(const OhosPrintManager&) = delete;
  OhosPrintManager& operator=(const OhosPrintManager&) = delete;

  ~OhosPrintManager() override;

  static void BindPrintManagerHost(
      mojo::PendingAssociatedReceiver<printing::mojom::PrintManagerHost>
          receiver,
      content::RenderFrameHost* rfh);

  // printing::PrintManager:
  void PdfWritingDone(int page_count) override;

  void DidShowPrintDialog() override;

  // Updates the parameters for printing.
  void UpdateParam(std::unique_ptr<printing::PrintSettings> settings,
                   int file_descriptor,
                   PdfWritingDoneCallback callback);
  bool PrintNow();
  void PrintPage(bool isApplication);
  void PrintPageImpl(bool isApplication);
  void DidDispatchPrintEvent(bool isBefore);
  void DidDispatchPrintEventImpl(bool isBefore);
  void SetPrintAttrs(const PrintAttrs printAttrs);
  void RunPrintRequestedCallback(const std::string& jobId);
  void SetToken(void* token);
  void CreateWebPrintDocumentAdapter(const CefString& jobName, void** webPrintDocumentAdapter);

 private:
  friend class content::WebContentsUserData<OhosPrintManager>;

  explicit OhosPrintManager(content::WebContents* contents);

  // mojom::PrintManagerHost:
  void DidPrintDocument(printing::mojom::DidPrintDocumentParamsPtr params,
                        DidPrintDocumentCallback callback) override;
  void GetDefaultPrintSettings(
      GetDefaultPrintSettingsCallback callback) override;
  void ScriptedPrint(printing::mojom::ScriptedPrintParamsPtr params,
                     ScriptedPrintCallback callback) override;
  void PrintRequested(PrintRequestedCallback callback) override;
  void CheckCancel(CheckCancelCallback callback) override;

  static void OnDidPrintDocumentWritingDone(
      const PdfWritingDoneCallback& callback,
      DidPrintDocumentCallback did_print_document_cb,
      uint32_t page_count);

  std::unique_ptr<printing::PrintSettings> CreatePdfSettings(
      const printing::PageRanges& page_ranges);

  void OnScriptedPrint();
  std::string GetHtmlTitle();
  std::string RemoveProtocol(const std::string& url);

  std::unique_ptr<printing::PrintSettings> settings_;

  uint32_t fd_ = 0;
  uint32_t width_ = 8270;
  uint32_t height_ = 11690;
  int dpi_ = 300;  // DPI (Dots Per Inch)
  void* token_ = nullptr;
  bool cancel_ = false;
  static std::unordered_map<std::string, PrintAttrs> printAttrsMap_;
  static std::string printJobId_;
  PrintRequestedCallback printRequestedCallback_;

  WEB_CONTENTS_USER_DATA_KEY_DECL();
};

}  // namespace printing

#endif  // OHOS_PRINT_MANAGER_H_
