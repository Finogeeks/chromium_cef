// Copyright (c) 2022 Marshall A. Greenblatt. All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//    * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//    * Neither the name of Google Inc. nor the name Chromium Embedded
// Framework nor the names of its contributors may be used to endorse
// or promote products derived from this software without specific prior
// written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// ---------------------------------------------------------------------------
//
// The contents of this file must follow a specific format in order to
// support the CEF translator tool. See the translator.README.txt file in the
// tools directory for more information.
//

#ifndef CEF_INCLUDE_CEF_DATA_BASE_H_
#define CEF_INCLUDE_CEF_DATA_BASE_H_
#pragma once

#include <vector>
#include "include/cef_base.h"

///
// Class used for managing web data base.
///
/*--cef(source=library,no_debugct_check)--*/
class CefDataBase : public virtual CefBaseRefCounted {
 public:
  ///
  // Returns the global data base instance.
  ///
  /*--cef()--*/
  static CefRefPtr<CefDataBase> GetGlobalDataBase();

  ///
  // clear all http auth data
  ///
  /*--cef()--*/
  virtual void DeleteHttpAuthCredentials() = 0;

  ///
  // get whether there has any http auth data.
  ///
  /*--cef()--*/
  virtual bool ExistHttpAuthCredentials() = 0;

  ///
  // save http auth data.
  ///
  /*--cef()--*/
  virtual void SaveHttpAuthCredentials(const CefString& host,
                                       const CefString& realm,
                                       const CefString& username,
                                       const char* password) = 0;

  ///
  // get http auth data by host and realm.
  ///
  /*--cef()--*/
  virtual void GetHttpAuthCredentials(
      const CefString& host,
      const CefString& realm,
      CefString& username,
      char* password,
      uint32_t passwordSize) = 0;

  ///
  // gets whether the instance holds the specified permissions for the specified
  // source.
  ///
  /*--cef()--*/
  virtual bool ExistPermissionByOrigin(const CefString& origin, int type) = 0;

  ///
  // get specifies permission type result by origin.
  ///
  /*--cef()--*/
  virtual bool GetPermissionResultByOrigin(const CefString& origin,
                                           int type,
                                           bool& result) = 0;

  ///
  // set specifies permission type by origin.
  ///
  /*--cef()--*/
  virtual void SetPermissionByOrigin(const CefString& origin,
                                     int type,
                                     bool result) = 0;

  ///
  // delete specifies permission type by origin.
  ///
  /*--cef()--*/
  virtual void ClearPermissionByOrigin(const CefString& origin, int type) = 0;

  ///
  // delete all specifies permission type.
  ///
  /*--cef()--*/
  virtual void ClearAllPermission(int type) = 0;

  ///
  // obtains all origins of a specified permission type.
  ///
  /*--cef()--*/
  virtual void GetOriginsByPermission(int type,
                                      std::vector<CefString>& origins) = 0;
};

#endif  // CEF_INCLUDE_CEF_DATA_BASE_H_